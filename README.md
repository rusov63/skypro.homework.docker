## Модуль 8 от SkyPro. Docker. Для работы используется Linux Manjaro

## 27.2. Docker Compose. Домашнее задание
### (ветка 27.2._HW_Docker_Compose)

### Реализовано в проекте:

#### 1. Установлен Docker, docker compose. 
        - sudo pacman -Syu
        - sudo pacman -S docker
            - docker --version
        - sudo pacman -S docker compose 
            - docker-compose --version

#### 2. Создан и описан в корне проекта: Dockerfile, docker-compose.yaml
        - python:3.11-slim-buster

#### 3. Собран образ и запущен контейнер
        - docker-compose build
        - docker-compose up

#### 4. Запуск проекта одной командой
        - docker-compose up -d --build

#### 5. Проверяем проект на работоспособность
        - localhost:8000

___________________________________________________________________________________________________________________

## 28.1 Деплой и CI/CD. Домашнее задание
### (ветка 28_1_HW_Деплой_and_CI/CD)

### Реализовано в проекте:

#### 1. Зарегестрирован облачный сервер https://timeweb.cloud/, 1 CPU • 2 RAM • 30гб NVME • Ubuntu 22.04
        - сервер 83.222.9.119

#### 2. Клонирован проект на https://gitlab.com/rusov63/skypro.homework.docker, созданы Runners, Pipelines. Установлен на сервер Gitlab-runner. В проекте на сервере в ручную установлено вир.окружение, библиотеки из requirements.txt. Созданы файлы Gunicorn, NGINX
        - sudo -E apt-get install gitlab-runner
        - gitlab-runner register

#### 3. Описан файл gitlab-ci.yml для проекта.

#### 4. IP-адрес, по которому можно открыть проект
        - http://83.222.9.119/



